package pl.sonpm.kotlin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class KotlinActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val operationButtonClickListener = OperationButtonClickListener()

        add.setOnClickListener(operationButtonClickListener)
        subtract.setOnClickListener(operationButtonClickListener)
        multiply.setOnClickListener(operationButtonClickListener)
        divide.setOnClickListener(operationButtonClickListener)

        switchActivity.setOnClickListener({
            val intent = Intent(this, JavaActivity::class.java)
            startActivity(intent)
        })
    }

    inner private class OperationButtonClickListener : View.OnClickListener {
        override fun onClick(view: View?) {
            val leftOperand: Double
            val rightOperand: Double
            val expressionResult: Double?

            try {
                leftOperand = number1.text.toString().toDouble()
                rightOperand = number2.text.toString().toDouble()
            } catch (e: NumberFormatException) {
                resultDescription.text = ""
                result.text = ""
                return
            }

            expressionResult = when (view?.id) {
                R.id.add -> leftOperand + rightOperand
                R.id.subtract -> leftOperand - rightOperand
                R.id.multiply -> leftOperand * rightOperand
                R.id.divide -> leftOperand / rightOperand
                else -> null
            }

            resultDescription.text = when (expressionResult?.toInt()) {
                in 0..10 -> getString(R.string.label_result_description_low)
                in 11..80 -> getString(R.string.label_result_description_medium)
                in 81..150 -> getString(R.string.label_result_description_high)
                else -> getString(R.string.label_result_description_unknown)
            }

            result.text = getString(R.string.label_result, expressionResult)
        }
    }
}