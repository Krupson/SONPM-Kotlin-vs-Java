package pl.sonpm.kotlin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class JavaActivity extends AppCompatActivity {
    private EditText number1;
    private EditText number2;
    private Button add;
    private Button subtract;
    private Button multiply;
    private Button divide;
    private TextView result;
    private TextView resultDescription;
    private Button switchActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        number1 = findViewById(R.id.number1);
        number2 = findViewById(R.id.number2);
        add = findViewById(R.id.add);
        subtract = findViewById(R.id.subtract);
        multiply = findViewById(R.id.multiply);
        divide = findViewById(R.id.divide);
        result = findViewById(R.id.result);
        resultDescription = findViewById(R.id.resultDescription);
        switchActivity = findViewById(R.id.switchActivity);

        final OperationButtonClickListener operationButtonClickListener = new OperationButtonClickListener();

        add.setOnClickListener(operationButtonClickListener);
        subtract.setOnClickListener(operationButtonClickListener);
        multiply.setOnClickListener(operationButtonClickListener);
        divide.setOnClickListener(operationButtonClickListener);

        switchActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private class OperationButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            final Double leftOperand;
            final Double rightOperand;
            final Double expressionResult;

            try {
                leftOperand = Double.parseDouble(number1.getText().toString());
                rightOperand = Double.parseDouble(number2.getText().toString());
            } catch (NumberFormatException e) {
                resultDescription.setText("");
                result.setText("");
                return;
            }

            switch (view.getId()) {
                case R.id.add:
                    expressionResult = leftOperand + rightOperand;
                    break;
                case R.id.subtract:
                    expressionResult = leftOperand - rightOperand;
                    break;
                case R.id.multiply:
                    expressionResult = leftOperand * rightOperand;
                    break;
                case R.id.divide:
                    expressionResult = leftOperand / rightOperand;
                    break;
                default:
                    expressionResult = null;
            }

            if (expressionResult == null) {
                return;
            } else if (expressionResult >= 0 && expressionResult <= 10) {
                resultDescription.setText(getString(R.string.label_result_description_low));
            } else if (expressionResult >= 11 && expressionResult <= 80) {
                resultDescription.setText(getString(R.string.label_result_description_medium));
            } else if (expressionResult >= 81 && expressionResult <= 150) {
                resultDescription.setText(getString(R.string.label_result_description_high));
            } else {
                resultDescription.setText(getString(R.string.label_result_description_unknown));
            }

            result.setText(getString(R.string.label_result, expressionResult));
        }
    }
}